﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for ClassName
/// </summary>
public class ClassName
{
    public static void main()
    {
 
        char[] palindrome = {'r','o','b','o','r'};
        int size = palindrome.Length;

        for (int i=0; i<size; i++){
            if (palindrome[i] == palindrome[size-1-i]){
                if (i >= size-1-i) {
                    break;
                }
                continue;
            }
            else {
                Console.WriteLine("Not a palindrome");
                return;
            }
        }
        Console.WriteLine("Palindrome");
        return;
    }
}
